" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'

set encoding=utf-8

set t_Co=256

if has("nvim")
	call plug#begin('~/.local/share/nvim/plugged')
else
	call plug#begin('~/.vim/plugged')
endif

"Plug 'chrisbra/csv.vim'
"Plug 'dansomething/vim-eclim'
"Plug 'https://github.com/ycm-core/YouCompleteMe.git'
Plug 'preservim/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'rhysd/vim-clang-format'
Plug 'https://github.com/editorconfig/editorconfig-vim.git'
Plug 'https://github.com/nvie/vim-flake8.git'

"Plug 'https://github.com/dracula/vim'
"Plug 'https://github.com/w0ng/vim-hybrid'
"Plug 'https://github.com/cocopon/iceberg.vim'
"Plug 'https://github.com/abra/vim-obsidian.git'
Plug 'https://github.com/rafi/awesome-vim-colorschemes.git'
Plug 'https://github.com/flazz/vim-colorschemes.git'
Plug 'https://github.com/vim-scripts/ScrollColors.git'

" Unmanaged plugin (manually installed and updated)
"Plug '~/my-vim-plugins/recogneyes'

" Make sure you use single quotes

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
"Plug 'junegunn/vim-easy-align'

" Any valid git URL is allowed
"Plug 'https://github.com/junegunn/vim-github-dashboard.git'

" Multiple Plug commands can be written in a single line using | separators
"Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'

" On-demand loading
"Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
"Plug 'tpope/vim-fireplace', { 'for': 'clojure' }

" Using a non-master branch
"Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }

" Using a tagged release; wildcard allowed (requires git 1.9.2 or above)
"Plug 'fatih/vim-go', { 'tag': '*' }

" Plugin options
"Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }

" Plugin outside ~/.vim/plugged with post-update hook
"Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }

" Unmanaged plugin (manually installed and updated)
"Plug '~/my-prototype-plugin'

" Initialize plugin system
call plug#end()

if has("syntax")
  syntax on
endif

" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
set background=dark
hi Cursor guifg=white guibg=black
hi iCursor guifg=white guibg=white
set termguicolors
colorscheme bitterjug
"hi CursorLine   cterm=NONE ctermbg=darkred ctermfg=white
hi CursorLine   cterm=NONE ctermbg=darkred ctermfg=white guibg=Grey10
hi Visual guibg=#383838 gui=none
hi PMenu	cterm=NONE ctermbg=darkred ctermfg=white guibg=Grey12

set shiftwidth=8
set shiftwidth=0
set noexpandtab

autocmd FileType yaml setlocal shiftwidth=2 softtabstop=2 expandtab
autocmd FileType json setlocal shiftwidth=2 softtabstop=2 expandtab
autocmd FileType python setlocal shiftwidth=4 softtabstop=4 expandtab
autocmd FileType cpp setlocal shiftwidth=4 softtabstop=4 expandtab
autocmd FileType sh setlocal shiftwidth=8 softtabstop=0 noexpandtab
autocmd FileType cmake setlocal shiftwidth=4 softtabstop=4 expandtab

autocmd BufNewFile,BufRead *.jdebug set syntax=html
"autocmd BufReadPost *.jdebug setlocal shiftwidth=2 softtabstop=2 expandtab

" Make <SHIFT>-o fast (And possible other commands as well)
set timeout timeoutlen=5000 ttimeoutlen=100

" Swedish keyboard simplifictions
noremap ¤ ^
noremap § ^
noremap ½ $
noremap öö [[
noremap ää ]]
noremap öä []
noremap äö ][
nnoremap äc ]c
nnoremap öc [c
nnoremap äs ]s
nnoremap ös [s
noremap ÖÖ {{
noremap ÄÄ }}
noremap ö [
noremap ä ]
noremap , ;
noremap ; ,

nnoremap <F1> :bn<CR>
nnoremap <F2> :bp<CR>

map <silent> Å <C-]>

" NerdTree commands
nnoremap ¨n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>


set number
" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
set showcmd             " Show (partial) command in status line.
set showmatch           " Show matching brackets.
"set ignorecase         " Do case insensitive matching
"set smartcase          " Do smart case matching
set incsearch           " Incremental search
set autowrite           " Automatically save before commands like :next and :make
set hidden             " Hide buffers when they are abandoned
set mouse=a             " Enable mouse usage (all modes)
set cul                 " Enable underline
" guioptions -=m      " Removes the menubar
" guioptions -=T      " Removes the toolbar.

" turn hybrid line numbers on
set number relativenumber
set nu rnu

" Airline configurations
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

set diffopt+=internal,algorithm:patience

" Eclim configurations
let g:EclimCompletionMethod = 'omnifunc'
let g:ycm_extra_conf_globlist = ['~/project/*','!~/*']
"let g:ycm_filetype_specific_completion_to_disable = {
      "\ 'cpp': 1,
      "\ 'c': 1
      "\}
set visualbell
set t_vb=

